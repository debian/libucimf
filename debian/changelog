libucimf (2.3.8-9) UNRELEASED; urgency=medium

  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

 -- Ondřej Nový <onovy@debian.org>  Mon, 01 Oct 2018 10:06:29 +0200

libucimf (2.3.8-8) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8.
  * Bump compat to 10.
  * Update Vcs-* fields.
  * Fix GCC 6 compiling error. (Closes: #822699)

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Sat, 05 Nov 2016 10:06:48 +0800

libucimf (2.3.8-7) unstable; urgency=medium

  * Fix GCC 6 compiling error. (Closes: #811951)
  * Add ChangZhuo Chen as Uploaders.
  * Use debhelper (>= 9.20151219) to support dbgsym.
  * Bump Standard-Version to 3.9.6.
  * Update Homepage.
  * Use DEP5 copyright format.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Thu, 21 Jan 2016 22:51:33 +0800

libucimf (2.3.8-6) unstable; urgency=medium

  * Use autotools_dev for supporting new architectures (Closes: #727443)

 -- Aron Xu <aron@debian.org>  Wed, 27 Aug 2014 07:43:40 +0800

libucimf (2.3.8-5) unstable; urgency=low

  * Fix package description typos. Clsoes: #671900, #679697
  * Updated to compat 9, policy 3.9.4 and wrap-and-sort.
  * Adjust rules to multiarch and hardening. Closes: #722802
  * Adjust install path to multiarch.
  * Removed static library.

 -- Osamu Aoki <osamu@debian.org>  Sat, 14 Sep 2013 22:52:30 +0900

libucimf (2.3.8-4) unstable; urgency=low

  * Fix build failure with GCC 4.7. Closes: #667256.
    Thanks doko@d.o for the patch.

 -- Aron Xu <aron@debian.org>  Tue, 17 Apr 2012 08:26:40 +0200

libucimf (2.3.8-3) unstable; urgency=low

  * debian/rules:
    - Wipe out unneeded .la file.

 -- Aron Xu <aron@debian.org>  Sat, 30 Jul 2011 15:27:06 +0800

libucimf (2.3.8-2) unstable; urgency=low

  * debian/libucimf-dev.install:
    - Clean the dependency_libs out of .la files at build time,
      per Policy 10.2 (Closes: #620617).
  * debian/control:
    - Move fbterm and jfbterm from Depends to Recommends.
    - Remove useless ${shlibs:Depends}.
    + Bump std-ver: 3.9.2.
  * debian/patches/fix_manpages.patch:
    - Fix minor problems in manpages.

 -- Aron Xu <happyaron.xu@gmail.com>  Sat, 09 Apr 2011 13:23:16 +0800

libucimf (2.3.8-1) unstable; urgency=low

  * New upstream release:
    + Can detect multiple input methods when installed.
  * debian/control:
    + Add DMUA
    + Add "dialog" to Recommends of ucimf package.
  * debian/*.1:
    - Removed, merged upstream.
  * debian/ucimf.manpages:
    - Change accordingly.
  * debian/ucimf.examples:
    + Enlist examples.
  * debian/ucimf.install:
    - Remove items that not needed any more.

 -- Aron Xu <happyaron.xu@gmail.com>  Tue, 22 Feb 2011 03:27:00 +0800

libucimf (2.3.7-2) unstable; urgency=low

  * debian/control:
      Bump std-ver to 3.9.1,
      Remove Build-Depends on libmpfr-dev,
      Add Vcs-Bzr and Vcs-Browser fields.

 -- Aron Xu <happyaron.xu@gmail.com>  Wed, 08 Sep 2010 10:16:00 +0800

libucimf (2.3.7-1) unstable; urgency=low

  * New upstream release:
      Fixing build failures on ia64 and sparc,
      Tuning Makefile and scripts,
      More features added.
  * Update manpages:
      Fixing the hyphen-used-as-minus-sign,
      Fixing two typos.
  * debian/control:
      Bump std-ver to 3.9.0,
      Add fbterm (>= 1.5) requirement to binary package ucimf,
      Remove duplicate section info,
      Fixing duplicate-short-description.
  * debian/docs: Remove empty NEWS from it.
  * debian/copyright: Update copyright info.

 -- Aron Xu <happyaron.xu@gmail.com>  Sat, 10 Jul 2010 18:55:22 +0800

libucimf (2.3.5-1) unstable; urgency=low

   * Upstream new release, with a lot of bug fixes and new features.

 -- Aron Xu <happyaron.xu@gmail.com>  Fri, 25 Jun 2010 20:13:22 +0800

libucimf (2.2.9.1-2) unstable; urgency=low

  * Make it build on kfreebsd-* archs.
  * debian/copyright: Add include/imf/imf.h 's license.

 -- Aron Xu <happyaron.xu@gmail.com>  Tue, 1 Jun 2010 19:14:11 +0800

libucimf (2.2.9.1-1) unstable; urgency=low

  * Initial release (Closes: #567247)

 -- Aron Xu <happyaron.xu@gmail.com>  Fri, 29 Jan 2010 15:54:32 +0800
